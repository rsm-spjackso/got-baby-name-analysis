# Game of Thrones Baby Name Analysis
library(tidyverse)

filelist <- list.files(path = "./namesbystate", pattern = ".*.TXT")
path <- rep("./namesbystate/", 51)
filelist <- paste(path, filelist, sep = "")


#assuming tab separated values with a header    
datalist <- lapply(filelist, function(x)read_csv(x, col_names = FALSE)) 

#assuming the same header/columns for all files
dataframe <- do.call("rbind", datalist) 

# add header
colnames(dataframe) <- c("state", "gender", "year", "name", "number")

# F for female was read in as False
dataframe$gender[dataframe$gender == "FALSE"] <- "Female"

# M for male was read in as NA
dataframe$gender[is.na(dataframe$gender)] <- "Male"

table(dataframe$gender)
length(dataframe$gender)
str(dataframe)

rm(filelist,path)

