# US Census/Game of Thrones Baby Name Analysis

MGT 452 Class Project for Team 9 Blue Ocean

Analysis of US Census baby names for each state each year 1910-2017
- Geographic
- Word Cloud
- Animation
- K-Means
- Factor Analysis

https://catalog.data.gov/dataset/baby-names-from-social-security-card-applications-data-by-state-and-district-of-

![](Female_10.png)

![](women_legend.gif)

![](men_legend.gif)