# load library
library(tidyverse)

# load name data
dataframe <- readRDS("./BabyNames.rds")


# read in state capital lat/lon positions
caps <- read_csv("capital_lat_lon.csv")

# read in GOT character names
got_names <- read_csv("got_char_names.csv")
got_names <- got_names[,1]
# uncomment below to write out names to csv file
# write_csv(got_names, "cot_char_names.csv")

# for each state, for each gender, count up the total number per name, then return the top name
# create dataframe x by takeing dataframe then
top_names <- dataframe %>%
  # groupby gender, state, name then
  group_by(gender, state, name) %>%
  # summarise sum of number of names then
  summarise(n = sum(number)) %>%
  # arrange in descending order within each group then
  arrange(desc(n)) %>%
  # return the highest n for each group
  slice(1)

# dataframe of second most popular names by state
second_names <- dataframe %>%
  # groupby gender, state, name then
  group_by(gender, state, name) %>%
  # summarise sum of number of names then
  summarise(n = sum(number)) %>%
  # arrange in descending order within each group then
  arrange(desc(n)) %>%
  # return the highest n for each group
  slice(2)

third_names <- dataframe %>%
  # groupby gender, state, name then
  group_by(gender, state, name) %>%
  # summarise sum of number of names then
  summarise(n = sum(number)) %>%
  # arrange in descending order within each group then
  arrange(desc(n)) %>%
  # return the highest n for each group
  slice(3)


top_names <- left_join(top_names, caps, by = "state" )
second_names <- left_join(second_names, caps, by = "state" )
third_names <- left_join(third_names, caps, by = "state" )



# uncomment below to output files
#write_csv(third_names, "top_names.csv")
#write_csv(third_names, "second_names.csv")
#write_csv(third_names, "third_names.csv")

# clear memory
#rm(top_names, second_names, third_names, got_names, datalist)
