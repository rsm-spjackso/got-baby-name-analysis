library(tidyverse)
library('RColorBrewer')
## Female

dataframe <- read_rds('BabyNames.rds')
dataframe_F <- dataframe %>%
  filter(gender == 'Female')

F_popular <- dataframe_F %>%
  group_by(name) %>%
  summarize(sum_ = sum(number))%>%
  arrange(desc(sum_))

top5<- slice(F_popular,1:5)$name

## First 
Female_5 <- dataframe_F %>%
  filter(name %in% top5) %>%
  group_by(year,name) %>%
  summarize(sum_ = sum(number)) %>%
  ggplot(aes(x = year, y = sum_,col = name))+
  geom_line(size = 1.5)+ 
  scale_color_brewer(palette = 'Set3')+
  labs(x = 'Year', y = 'Number',title = '5 Most Popular Female Names in US')

ggsave(filename = 'Female_5.png',height =6 ,width = 8,dpi = 150,units = 'in',device = 'png')

## Second

popu_F <- dataframe_F %>%
  group_by(year) %>%
  summarize(population_F= sum(number))

Female_norm <- dataframe_F %>%
  group_by(year,name) %>%
  summarize(sum_ = sum(number)) %>%
  left_join(popu_F, by = 'year') %>%
  mutate(pro = sum_/population_F) %>%
  group_by(year) %>%
  arrange(desc(pro)) %>%
  slice(1)

top10_norm <- unique(Female_norm$name)

Female_10 <- dataframe_F %>%
  group_by(year,name) %>%
  summarize(sum_ = sum(number)) %>%
  left_join(popu_F, by = 'year') %>%
  mutate(pro = sum_/population_F) %>%
  group_by(year) %>%
  arrange(desc(pro)) %>%
  filter(name %in% top10_norm) %>%
  ggplot(aes(x = year, y = pro, fill = factor(name)))+
  geom_bar(stat="identity")+
  scale_fill_brewer(palette = "Spectral")+
  labs(x = 'Year', y = 'Proportion',title = '10 Most Popular Female Names in US')

ggsave(filename = 'Female_10.png',height =6 ,width = 8,dpi = 150,units = 'in',device = 'png')

## Third
dataframe_M <- dataframe %>%
  filter(gender == 'Male')

M_popular <- dataframe_M %>%
  group_by(name) %>%
  summarize(sum_ = sum(number))%>%
  arrange(desc(sum_))


top5_M<- slice(M_popular,1:5)$name

Male_5 <- dataframe_M %>%
  filter(name %in% top5_M) %>%
  group_by(year,name) %>%
  summarize(sum_ = sum(number)) %>%
  ggplot(aes(x = year, y = sum_,col = name))+
  geom_line(size = 1.5)+
  scale_color_brewer(palette = 'Set3')+
  labs(x = 'Year', y = 'Number',title = '5 Most Popular Male Names in US')

ggsave(filename = 'Male_5.png',height =6 ,width = 8,dpi = 150,units = 'in',device = 'png')

##Forth
popu_M<- dataframe_M %>%
  group_by(year) %>%
  summarize(population_M= sum(number))


Male_norm <- dataframe_M %>%
  group_by(year,name) %>%
  summarize(sum_ = sum(number)) %>%
  left_join(popu_M, by = 'year') %>%
  mutate(pro = sum_/population_M) %>%
  group_by(year) %>%
  arrange(desc(pro)) %>%
  slice(1)
top8_M_norm <- unique(Male_norm$name)

Male_8 <- dataframe_M %>%
  group_by(year,name) %>%
  summarize(sum_ = sum(number)) %>%
  left_join(popu_M, by = 'year') %>%
  mutate(pro = sum_/population_M) %>%
  group_by(year) %>%
  arrange(desc(pro)) %>%
  filter(name %in% top10_M_norm)  %>%
  ggplot(aes(x = year, y = pro, fill = factor(name)))+
  geom_bar(stat="identity")+
  scale_fill_brewer(palette = "Spectral")+
  labs(x = 'Year', y = 'Proportion',title = '8 Most Popular Male Names in US')+
  
  Male_8
ggsave(filename = 'Male_8.png',height =6 ,width = 8,dpi = 150,units = 'in',device = 'png')

## Fifth
MSBA = c('Jackie','Sean','Gunnar','Marc','Trevor','Samuel','Noora','Albert','Robert','Joshua')
popu<- dataframe %>%
  group_by(year) %>%
  summarize(population= sum(number))

MSBA_name <- dataframe %>%
  group_by(year,name) %>%
  summarize(sum_ = sum(number)) %>%
  left_join(popu, by = 'year') %>%
  mutate(pro = sum_/population) %>%
  group_by(year) %>%
  arrange(desc(pro)) %>%
  filter(name %in% MSBA) %>%
  ggplot(aes(x = year, y = pro, fill = factor(name)))+
  geom_bar(stat="identity")+
  scale_fill_brewer(palette = 'Set3')+
  labs(x = 'Year', y = 'Proportion',title = 'MSBA 2019 Names Trend')
MSBA_name
ggsave(filename = 'MSBA_name.png',height =6 ,width = 8,dpi = 150,units = 'in',device = 'png')

## Sixth
uni <- dataframe %>%
  group_by(name,gender) %>%
  summarize(sum_ = sum(number)) %>%
  spread(gender,sum_)

uni[is.na(uni)] <- 0

uni <- uni %>%
  mutate(all = sum(Female + Male),F_p = Female/all,M_p = Male/all,diff = abs(F_p-M_p)) %>%
  arrange(diff)

pop_uni <- uni %>%
  filter(all > 10000 & diff < 0.107) %>%
  filter(name != 'Unknown') %>%
  arrange(desc(all))

pop10_uni <- pop_uni$name

## trend over uniname 
uniname <- dataframe %>%
  filter(name %in% pop10_uni) %>%
  group_by(year,name,gender) %>%
  summarize(sum_ = sum(number)) %>%
  arrange(desc(sum_))

uniname$name <- factor(uniname$name,levels = pop10_uni)

uni_name <- uniname %>%
  ggplot(aes(x= year,y=sum_,col = gender ))+
  geom_line(size = 2)+
  facet_wrap(~name) +
  labs(x = 'Year', y = 'Number', title ='10 Most Popular Unisex Name' )+
  theme(strip.text.x = element_text(size=10,face = 'bold'),
        strip.text.y = element_text(size=10,face = 'bold'))

ggsave(filename = 'uni_name.png',height =6 ,width = 8,dpi = 150,units = 'in',device = 'png')

