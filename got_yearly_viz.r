# GOT yearly exploratory analysis viz

# first read in babynames
dataframe <- readRDS("./BabyNames.rds")

# load library
library(tidyverse)
library(gganimate)


got <- read_csv("./got_char_names.csv")

got_names <- dataframe %>%
  filter(name %in% got$got_name) 


top_got_yearly <- got_names %>%
  # group_by gender, state, name then
  group_by(year, gender, state, name) %>%
  # summarise sum of number of names then
  summarise(n = sum(number)) %>%
  # arrange in descending order within each group then
  arrange(desc(n)) %>%
  # return the highest n for each group
  slice(1)


# read in state capital lat/lon positions
caps <- read_csv("capital_lat_lon.csv")

# join in state capital lat/lon positions
top_got_yearly <- left_join(top_got_yearly, caps, by = "state" )
rm(caps)

# load state map data
states <- map_data("state")

men <- top_got_yearly %>%
  filter(gender == "Male") %>%
  filter(state != "AK" & state != "HI")

women <- top_got_yearly %>%
  filter(gender == "Female") %>%
  filter(state != "AK" & state != "HI")

# code below places points with sizes depending on number of names for each state at capital lat/lon for men
x <- ggplot() +
  # geom polygon draws the map for the US lower 48 states
  geom_polygon(data=states, aes(x=long, y=lat, group = group),color="white", fill="grey92" ) + 
  # geom point draws a point at the lat/lon of each state capital with color based on name and size based on number of each name
  geom_point(data=men, aes(x=lon, y=lat, size = n, color = name)) + 
  # scale size of the points on the map
  scale_size(name="", range = c(2, 20)) +
  # legend text
  guides(size=guide_legend("Frequency of most Popular Name")) +
  # overall theme of map
  theme_void() +
  #theme(legend.position = "none")+ 
  # gganimate specifics
  labs(title = 'Year: {frame_time}') +
  transition_time(year) +
  ease_aes('linear')


gganimate::animate(x, fps = 2, height = 600, width = 800)


# code below places points with sizes depending on number of names for each state at capital lat/lon for women
y <- ggplot() +
  # geom polygon draws the map for the US lower 48 states
  geom_polygon(data=states, aes(x=long, y=lat, group = group),color="white", fill="grey92" ) + 
  # geom point draws a point at the lat/lon of each state capital with color based on name and size based on number of each name
  geom_point(data=women, aes(x=lon, y=lat, size = n, color = name)) + 
  # scale size of the points on the map
  scale_size(name="", range = c(2, 20)) +
  # legend text
  guides(size=guide_legend("Frequency of most Popular Name")) +
  # overall theme of map
  theme_void() +
  #theme(legend.position = "none")+ 
  # gganimate specifics
  labs(title = 'Year: {frame_time}') +
  transition_time(year) +
  ease_aes('linear')

gganimate::animate(y, fps = 2, height = 600, width = 800)
