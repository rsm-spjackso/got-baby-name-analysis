# map vis


library(ggplot2)
library(tidyverse)

# read in top names data
top_names <- read_csv("top_names.csv")

# read in second names data
second_names <- read_csv("second_names.csv")

# read in third names data
third_names <- read_csv("third_names.csv")

# read in state map data for plots
states <- map_data("state")

men1 <- top_names %>%
  filter(gender == "Male") %>%
  filter(state != "AK" & state != "HI")

women1 <- top_names %>%
  filter(gender == "Female") %>%
  filter(state != "AK" & state != "HI")


# code below places points with sizes depending on number of names for each state at capital lat/lon
m1 <- ggplot() + geom_polygon(data=states, aes(x=long, y=lat, group = group),color="white", fill="grey92" ) + 
  geom_point(data=men1, aes(x=lon, y=lat, size = n, color = name)) + 
  scale_size(name="", range = c(1, 10)) + 
  guides(size=guide_legend("Top Male Frequency")) +
  theme(legend.key.width=unit(3,"cm")) +
  theme_void()

# output plot
ggsave("m1.png", height = 4, width = 7, units = "in", device = "png")

# women top names over 100 years
w1 <- ggplot() + geom_polygon(data=states, aes(x=long, y=lat, group = group),color="white", fill="grey92" ) + 
  geom_point(data=women1, aes(x=lon, y=lat, size = n, color = name)) + 
  scale_size(name="", range = c(1, 10)) + 
  guides(size=guide_legend("Top Female Frequency")) +
  theme(legend.key.width=unit(3,"cm")) +
  theme_void()

# output plot
ggsave("w1.png", height = 4, width = 7, units = "in", device = "png")


# second most popular names by state
men2 <- second_names %>%
  filter(gender == "Male") %>%
  filter(state != "AK" & state != "HI")

women2 <- second_names %>%
  filter(gender == "Female") %>%
  filter(state != "AK" & state != "HI")

# code below places points with sizes depending on number of names for each state at capital lat/lon
m2 <- ggplot() + geom_polygon(data=states, aes(x=long, y=lat, group = group),color="white", fill="grey92" ) + 
  geom_point(data=men2, aes(x=lon, y=lat, size = n, color = name)) + 
  scale_size(name="", range = c(1, 10)) + 
  guides(size=guide_legend("2nd Male Frequency")) +
  theme(legend.key.width=unit(3,"cm")) +
  theme_void()

# output plot
ggsave("m2.png", height = 4, width = 7, units = "in", device = "png")


w2 <- ggplot() + geom_polygon(data=states, aes(x=long, y=lat, group = group),color="white", fill="grey92" ) + 
  geom_point(data=women2, aes(x=lon, y=lat, size = n, color = name)) + 
  scale_size(name="", range = c(1, 10)) + 
  guides(size=guide_legend("2nd Female Frequency")) +
  theme(legend.key.width=unit(3,"cm")) +
  theme_void()

# output plot
ggsave("w2.png", height = 4, width = 7, units = "in", device = "png")


# third most popular names by state
men3 <- third_names %>%
  filter(gender == "Male") %>%
  filter(state != "AK" & state != "HI")

women3 <- third_names %>%
  filter(gender == "Female") %>%
  filter(state != "AK" & state != "HI")

# code below places points with sizes depending on number of names for each state at capital lat/lon
m3 <- ggplot() + geom_polygon(data=states, aes(x=long, y=lat, group = group),color="white", fill="grey92" ) + 
  geom_point(data=men3, aes(x=lon, y=lat, size = n, color = name)) + 
  scale_size(name="", range = c(1, 10)) + 
  guides(size=guide_legend("3rd Male Frequency")) +
  theme(legend.key.width=unit(3,"cm")) +
  theme_void()

# output plot
ggsave("m3.png", height = 4, width = 7, units = "in", device = "png")


w3 <- ggplot() + geom_polygon(data=states, aes(x=long, y=lat, group = group),color="white", fill="grey92" ) + 
  geom_point(data=women3, aes(x=lon, y=lat, size = n, color = name)) + 
  scale_size(name="", range = c(1, 10)) + 
  guides(size=guide_legend("3rd Female Frequency")) +
  theme(legend.key.width=unit(3,"cm")) +
  theme_void()

# output plot
ggsave("w3.png", height = 4, width = 7, units = "in", device = "png")

