# word cloud animation

library("tm")
library("SnowballC")
library("wordcloud")
library("RColorBrewer")
library('wordcloud2')
library('tidyverse')
library(gganimate)

dataframe <- readRDS('BabyNames.rds')

top_names_us_yearly <- dataframe %>%
  # change year to integer
  mutate(year = as.integer(year)) %>%
  # groupby gender, state, name then
  group_by(year, gender, name) %>%
  # summarise sum of number of names then
  summarise(n = sum(number)) %>%
  # arrange in descending order within each group then
  arrange(desc(n)) %>%
  # take top 10 each year
  slice(1:10)

# seperate men and women datasets
men <- top_names_us_yearly %>%
  filter(gender == "Male")

women <- top_names_us_yearly %>%
  filter(gender == "Female")

# unfortunately the code below does not work
# it appears that wordclouds are not the same format as ggplot 2 graphics
# and thus are incompatible 
x <- wordcloud(men$name,
               men$n,
               scale=c(5,0.5), 
               min.freq=1000,
               word.freq =100,
               random.color= TRUE,
               colors=brewer.pal(8,"Dark2")) +
               transition_time(year) + 
               ease_aes('linear')

gganimate::animate(x, fps = 2)
