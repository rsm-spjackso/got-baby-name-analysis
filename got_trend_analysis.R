library(tidyverse)

all_names <- readRDS("./BabyNames.rds")
got <- read_csv("./got_char_names.csv")


got_names <- all_names %>%
  filter(name %in% got$got_name)


top_got_names <- got_names %>%
  filter(year > 2000) %>%
  group_by(name) %>%
  summarise(n = sum(number)) %>%
  arrange(-n)


time_trend_plot <- got_names %>%
  filter(year > 2000) %>%
  filter(name %in% top_got_names$name) %>%
  group_by(name, year) %>%
  summarise(n = sum(number)) %>%
  ggplot(aes(x=year,y=n, group = name)) + geom_line() + facet_wrap(~name, nrow = 2) +
  labs(title='Total Number of GOT Baby Names by Year',
       x='Year',
       y='Number of GOT Baby Names')
time_trend_plot

time_trend_plot_2 <- got_names %>%
  filter(year > 2000) %>%
  filter(name %in% c('Jon', 'Arya')) %>%
  group_by(name, year) %>%
  summarise(n = sum(number)) %>%
  ggplot(aes(x=year,y=n, group = name)) + geom_line() + facet_wrap(~name, nrow = 2) +
  labs(title='Total Number of GOT Baby Names by Year',
       x='Year',
       y='Number of GOT Baby Names')
time_trend_plot_2

time_trend_plot_3 <- got_names %>%
  filter(year > 2000) %>%
  filter(name %in% c('Catelyn', 'Daenerys')) %>%
  group_by(name, year) %>%
  summarise(n = sum(number)) %>%
  ggplot(aes(x=year,y=n, group = name)) + geom_line() + facet_wrap(~name, nrow = 2) +
  labs(title='Total Number of GOT Baby Names by Year',
       x='Year',
       y='Number of GOT Baby Names')
time_trend_plot_3


time_trend_plot_4 <- got_names %>%
  filter(year > 2000) %>%
  filter(name %in% c('Brienne', 'Gregor', 'Sansa', 'Tyrion')) %>%
  group_by(name, year) %>%
  summarise(n = sum(number)) %>%
  ggplot(aes(x=year,y=n, group = name)) + geom_line() + facet_wrap(~name, nrow = 2) +
  labs(title='Total Number of GOT Baby Names by Year',
       x='Year',
       y='Number of GOT Baby Names')
time_trend_plot_4
