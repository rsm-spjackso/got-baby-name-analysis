# cluster ggplot2 attempt

# this is currently non functional as of 12/3/18 at 330pm

library(dplyr) 
library(tidyr)
library(ggplot2)
library(gridExtra)
library(magrittr)
library(RColorBrewer)
library(Rtsne)


babynames <- readRDS('./BabyNames.rds')
babynames <- as_tibble(babynames) %>%
  select(-state) %>%
  group_by(gender, year, name) %>%
  summarise(n = sum(number))

## male

top_100_male <- babynames %>%
  filter(gender == "Male") %>%
  group_by(name) %>%
  summarise(n = sum(n)) %>%
  arrange(-n) %>%
  slice(1:100)

babywideM <- babynames %>%
  filter(gender == "Male", name %in% top_100_male$name)%>%
  select(name, year, n) %>%
  spread(year, n, fill=0)

babywideM <- data.frame(babywideM[,2:110 ])



rownames(babywideM)<- babywideM %>% .$name  #set rownames
babywideM %<>% select(-name) # remove name var.

set.seed(100)
resM.k <- kmeans(babywideM, 6)
table(resM.k$cluster)

group1M <- names(resM.k$cluster[resM.k$cluster==1])
group2M <- names(resM.k$cluster[resM.k$cluster==2])
group3M <- names(resM.k$cluster[resM.k$cluster==3])
group4M <- names(resM.k$cluster[resM.k$cluster==4])
group5M <- names(resM.k$cluster[resM.k$cluster==5])
group6M <- names(resM.k$cluster[resM.k$cluster==6])




# exmaple 
tsne_plot <- data.frame(x = tsne_out$Y[,1], y = tsne_out$Y[,2], col = iris_unique$Species)
ggplot(tsne_plot) + geom_point(aes(x=x, y=y, color=col))
