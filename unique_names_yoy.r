# number of unique names each year

unique_names_yearly <- dataframe %>%
  # change year to integer
  mutate(year = as.integer(year)) %>%
  # groupby gender, state, name then
  group_by(year, gender, state, name) %>%
  # summarise sum of number of names then
  summarise(n = sum(unique(name))) %>%
  # arrange in descending order within each group then
  arrange(desc(n))

